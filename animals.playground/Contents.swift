//
//  animals.playground
//  iOS Networking
//
//  Created by Jarrod Parkes on 09/30/15.
//  Copyright (c) 2015 Udacity. All rights reserved.
//

import Foundation
import UIKit


/* Path for JSON files bundled with the Playground */
var pathForAnimalsJSON = Bundle.main.path(forResource: "animals", ofType: "json")

/* Raw JSON data (...simliar to the format you might receive from the network) */
var rawAnimalsJSON = try? Data(contentsOf: URL(fileURLWithPath: pathForAnimalsJSON!))

/* Error object */
var parsingAnimalsError: NSError? = nil

/* Parse the data into usable form */
var parsedAnimalsJSON = try! JSONSerialization.jsonObject(with: rawAnimalsJSON!, options: .allowFragments) as! NSDictionary

//    print(parsedAnimalsJSON)

func parseJSONAsDictionary(_ dictionary: NSDictionary) {
    /* Start playing with JSON here... */
    //    let arrayOfPhotoDictionaries : [String:AnyObject]
    guard let photosDictionary = parsedAnimalsJSON["photos"] as? NSDictionary else {
        print("Cannot find key 'photos' in \(parsedAnimalsJSON)")
        return
    }
    
    guard let numAnimalsPhotos = photosDictionary["total"] as? Int else {
        print("Cannot find key 'total' in \(photosDictionary)")
        return
    }
    
    // Number of photos on json
    print(numAnimalsPhotos)
    
    // Counting itens on photos item
    guard let photosFromDictionary = photosDictionary["photo"] as? [[String:AnyObject]] else{
        print("Cant read field photo")
        return
    }
    /* How many photos are in the JSON data set? */
    print(photosFromDictionary.count)
    
    /* Whats is the index of the foto that contains text interrufftion ? */
    let searchString = "interrufftion"
    for (index,photo) in photosFromDictionary.enumerated(){
        //        print("\(index) , \(photo)")
        guard let commentDictionary = photo["comment"] as? [String:AnyObject] else {
            print("Cant find key 'comment' at \(photosFromDictionary)")
            return
        }
        
        guard let content = commentDictionary["_content"] as? String else {
            print("Can't find key '_content' in \(photo)")
            return
        }
        
        if content.range(of: searchString) != nil {
            print("Found string \(searchString) at position \(index)")
        }
        
        /* For the third photo in the array of photos, what animal is shown? */
        if let photoURL = photo["url_m"] as? String, index == 2 {
            print(photoURL)
        }
        
//        let imageUrl:URL = URL(string: photoUrl)!
//        let photoImageView: UIImageView!
//        guard let imageData = try? Data(contentsOf: imageUrl) else {
//            print("cant load image")
//            return
//        }
//
//        let i = UIImage(data: imageData)
//
//
        
    }
    
}


//extension UIImageView {
//    func load(url: URL) {
//        DispatchQueue.global().async { [weak self] in
//            if let data = try? Data(contentsOf: url) {
//                if let image = UIImage(data: data) {
//                    DispatchQueue.main.async {
//                        self?.image = image
//                    }
//                }
//            }
//        }
//    }
//}

parseJSONAsDictionary(parsedAnimalsJSON)
