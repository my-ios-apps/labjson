//
//  achievements.playground
//  iOS Networking
//
//  Created by Jarrod Parkes on 09/30/15.
//  Copyright (c) 2015 Udacity. All rights reserved.
//

import Foundation

/* Path for JSON files bundled with the Playground */
var pathForAchievementsJSON = Bundle.main.path(forResource: "achievements", ofType: "json")

/* Raw JSON data (...simliar to the format you might receive from the network) */
var rawAchievementsJSON = try? Data(contentsOf: URL(fileURLWithPath: pathForAchievementsJSON!))

/* Error object */
var parsingAchivementsError: NSError? = nil

/* Parse the data into usable form */
var parsedAchievementsJSON = try! JSONSerialization.jsonObject(with: rawAchievementsJSON!, options: .allowFragments) as! NSDictionary

func parseJSONAsDictionary(_ dictionary: NSDictionary) {
    /* Start playing with JSON here... */
    
    //For question 1
    var numAchievementsWithPointsGreatherThan10 = 0
    
    //For question 2
    var sumOfAchievementsPoints = 0.0
    var avgOfAchievementsPoints = 0.0
    
    //For question 3
    let titleMissionArrive = "Cool Running"
    var indexOfPreMission : Int!
    
    guard let arrayOfAchievements = parsedAchievementsJSON["achievements"] as? [[String:AnyObject]] else {
        print("Cannot find key 'achievements' in \(parsedAchievementsJSON)")
        return
    }
    
    //For question 4
    /*Store category children for matchMaking */
    var matchMakingIds : [Int] = []
    let categoryTitleSearch = "Matchmaking"
    
    guard let arrayOfCategories = parsedAchievementsJSON["categories"] as? [[String:AnyObject]] else {
        print("Cannot find key 'categories' in \(parsedAchievementsJSON)")
        return
    }
    //print(arrayOfCategories)
    
    /* Crie um dicionário para armazenar as contagens das categorias "Matchmaking" */
    var categoryCounts: [Int: Int] = [:]
    
    //print(arrayOfAchievements[410])
    /* Start loop of achievements */
    for (index,achievement)  in arrayOfAchievements.enumerated() {
        guard let points = achievement["points"] as? Int else {
            print("Can't find key points in \(arrayOfAchievements)")
            return
        }
        if points > 10 {
            numAchievementsWithPointsGreatherThan10 += 1
        }
        //        print (achievement)
        
        sumOfAchievementsPoints += Double(points)
        
        guard let title = achievement["title"] as? String else {
            print("Can't find key title in \(achievement)")
            return
        }
        
        if title == titleMissionArrive {
            indexOfPreMission = index
        }
        
        //Still for question 4
        /* Adicione à contagem de categorias */
        guard let categoryId = achievement["categoryId"] as? Int else {
            print("Cannot find key 'categoryId' in \(achievement)")
            return
        }
        
        /* A categoria tem uma chave no dicionário? Caso não, inicialize */
        if categoryCounts[categoryId] == nil {
            categoryCounts[categoryId] = 0
        }
        
        /* Adicione um à contagem de categorias */
        if let currentCount = categoryCounts[categoryId] {
            categoryCounts[categoryId] = currentCount + 1
        }
        
        
    }
    //For question 2
    avgOfAchievementsPoints = Double(sumOfAchievementsPoints) / Double(arrayOfAchievements.count)
    
    /* Question 1: How many achievements have a point value greater thans 10? */
    print("Achievements with points greater thans 10 \(numAchievementsWithPointsGreatherThan10)")
    
    /* Question 2: Whats is the average point value for achievements (round to the hounddreths place)? */
    print("Average of points value from achievemens: \(avgOfAchievementsPoints)")
    
    /* Question 3: Whats mission must you complete to get the "Cool Running" achievement (check description values)?*/
    guard  let titleMissionPreArrive = arrayOfAchievements[indexOfPreMission]["description"] as? String else {
        print ("Cant read the pre achievement of Cool Running")
        return
    }
    print("Required achievement to get \(titleMissionArrive) : \(titleMissionPreArrive)")
    
    /* Question 4: How many achievements belong to the "Matchmaking" category */
    for categoryDictionary in arrayOfCategories {
        if let title = categoryDictionary["title"] as? String , title == categoryTitleSearch {
            guard let children = categoryDictionary["children"] as? [NSDictionary] else {
                print("Cannot find key 'children' in \(categoryDictionary)")
                return
            }
            
            for child in children {
                guard let categoryId = child["categoryId"] as? Int else {
                    print("Cannot find key 'categoryId' in \(child)")
                    return
                }
                matchMakingIds.append(categoryId)
            }
        }
    }
    
    /*Calculate the achievements in category matchMaking */
    //print(categoryCounts)
    var matchmakingAchievementCount = 0
    for matchMakingId in matchMakingIds{
        if let categoryCount = categoryCounts[matchMakingId]{
                matchmakingAchievementCount+=categoryCount
        }
    }
    print("\(matchmakingAchievementCount) achievements belong to the \"\(categoryTitleSearch)\" category")
    
}

parseJSONAsDictionary(parsedAchievementsJSON)
