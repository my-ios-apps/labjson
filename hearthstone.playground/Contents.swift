//
//  hearthstone.playground
//  iOS Networking
//
//  Created by Jarrod Parkes on 09/30/15.
//  Copyright (c) 2015 Udacity. All rights reserved.
//

import Foundation

/* Path for JSON files bundled with the Playground */
var pathForHearthstoneJSON = Bundle.main.path(forResource: "hearthstone", ofType: "json")

/* Raw JSON data (...simliar to the format you might receive from the network) */
var rawHearthstoneJSON = try? Data(contentsOf: URL(fileURLWithPath: pathForHearthstoneJSON!))

/* Error object */
var parsingHearthstoneError: NSError? = nil

/* Parse the data into usable form */
var parsedHearthstoneJSON = try! JSONSerialization.jsonObject(with: rawHearthstoneJSON!, options: .allowFragments) as! NSDictionary

func parseJSONAsDictionary(_ dictionary: NSDictionary) {
    /* Start playing with JSON here... */
    
    var numCostRatioItens = 0
    var sumCostRatio : Double = 0.0
    
    var numCostForRarityItensDictionary = [String:Int]()
    var sumCostForRarityDictionary = [String:Int]()
    
    let rarities = ["Free", "Common"]
    
    var numOfMinionsWithCost5 = 0
    
    var numOfWeaponsWithDurability2 = 0
    
    /* Loop through the keys and initliaze those values to 0 */
    for rarity in rarities{
        numCostForRarityItensDictionary[rarity] = 0
        sumCostForRarityDictionary[rarity] = 0
    }
    
    /* Put cards in a Array */
    guard let arrayOfBasicSetCardDictionaries = parsedHearthstoneJSON["Basic"] as? [[String:AnyObject]] else {
        print("Cannot find key 'Basic' in \(parsedHearthstoneJSON)")
        return
    }
    
    for cardDictionary in arrayOfBasicSetCardDictionaries {
        //        print(cardDictionary)
        guard let cardType = cardDictionary["type"] as? String else {
            print("Cannot find key 'type' in \(cardDictionary)")
            return
        }
        
        /* Separe Minions cards */
        if cardType == "Minion" {
            
            // Reserve attack
            guard let attack = cardDictionary["attack"] as? Int else {
                print("Cannot find key 'attack' in \(cardDictionary)")
                return
            }
            
            // Reserve cost
            guard let manaCost = cardDictionary["cost"] as? Int else {
                print("Cannot find key 'cost' in \(cardDictionary)")
                return
            }
            
            /* How many minions have a cost of 5? */
            if manaCost == 5 {
                print("found a minion with cost of 5")
                numOfMinionsWithCost5+=1
            }
            
            /* Calculate stats-to-cost ratio if non-zero cost */
            if manaCost != 0 {
                
                numCostRatioItens += 1
                
                guard let health = cardDictionary["health"] as? Int else {
                    print("Cannot find key 'health' in \(cardDictionary)")
                    return
                }
                
                sumCostRatio += (Double(attack) + Double(health)) / Double(manaCost)
            }
            
            /* Get card cost based on rarity */
            guard let rarityForCard = cardDictionary["rarity"] as? String else {
                print("Cannot find key 'rarityForCard' in \(cardDictionary)")
                return
            }
            
            numCostForRarityItensDictionary[rarityForCard]! += 1
            sumCostForRarityDictionary[rarityForCard]! += manaCost
            
            /* How many minions have a "Battlecry" effect mentioned in their text? */
            if let cardText = cardDictionary["text"] as? String, cardText.range(of: "Battlecry") != nil {
                print("this minion has battlecry effect")
            }
        }
        
        if cardType == "Weapon" {
            /* How many weapons have a durability of 2? */
            if let durability = cardDictionary["durability"] as? Int, durability == 2  {
                print("found a weapon with  durability 2 ")
                numOfWeaponsWithDurability2+=1
            }
        }
    }
    
    /* Calculate the average stats-to-cost-ratio */
    print("\(sumCostRatio/Double(numCostRatioItens))")
    
    /* Calculate the average cost of minions based on rarity */
    for rarity in rarities {
        print("\(rarity): \(Double(sumCostForRarityDictionary[rarity]!) / Double(numCostForRarityItensDictionary[rarity]!))")
    }
    
    /*How many minions with cost 5*/
    print("Minions with cost 5: \(numOfMinionsWithCost5)")
    
    /*How many Weapons with Durability 2 */
    print("Weapons with Durability 2 : \(numOfWeaponsWithDurability2)")
}

parseJSONAsDictionary(parsedHearthstoneJSON)
